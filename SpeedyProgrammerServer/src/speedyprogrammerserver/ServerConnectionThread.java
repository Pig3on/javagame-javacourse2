/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package speedyprogrammerserver;

import Engine.GameInstance;
import java.io.IOException;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.LinkedList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author Pig3on
 */
public class ServerConnectionThread implements Runnable {

    ServerSocket serverSocket;
    Socket clientSocket;
    private LinkedList<GameInstance> gameInstances;

    public ServerConnectionThread(LinkedList<GameInstance> gameInstances) {
        this.gameInstances = gameInstances;
    }

    @Override
    public void run() {
        try {
            serverSocket = new ServerSocket(2355);
            while (true) {

                clientSocket = serverSocket.accept();
                ServerConnectionWorker work = new ServerConnectionWorker(clientSocket, gameInstances);
                Thread worker = new Thread(work);
                worker.setDaemon(true);
                worker.start();

            }
        } catch (IOException ex) {
            Logger.getLogger(ServerConnectionThread.class.getName()).log(Level.SEVERE, null, ex);
        }

    }

}
