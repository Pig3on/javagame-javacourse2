/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package speedyprogrammerserver;

import Engine.GameInstance;
import Enums.GameState;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.net.Socket;
import java.util.LinkedList;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author Pig3on
 */
public class ServerConnectionWorker implements Runnable {

    ObjectOutputStream oos;

    Socket clientSocket;

    private final LinkedList<GameInstance> gameInstances;

    public ServerConnectionWorker(Socket clientSocket, LinkedList<GameInstance> gameInstances) {
        this.clientSocket = clientSocket;
        this.gameInstances = gameInstances;
    }

    @Override
    public void run() {
        getLatestGame();
    }

    private void getLatestGame() {
        try {

            oos = new ObjectOutputStream(clientSocket.getOutputStream());

            for (GameInstance gameInstance : gameInstances) {

                if (gameInstance.getGameState() == GameState.REVIEW_READY) {
                    gameInstance.setGameState(GameState.TAKEN_FOR_REVIEW);
                    oos.writeObject(gameInstance);
                    break;
                }
            }

            clientSocket.close();

        } catch (IOException ex) {
            Logger.getLogger(ServerConnectionWorker.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

}
