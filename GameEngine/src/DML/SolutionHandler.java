/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package DML;

import CustomExceptions.InlineCompilerException;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.io.Writer;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLClassLoader;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Locale;
import javax.tools.DiagnosticCollector;
import javax.tools.JavaCompiler;
import javax.tools.JavaFileObject;
import javax.tools.StandardJavaFileManager;
import javax.tools.ToolProvider;

/**
 *
 * @author Pig3on
 */
public class SolutionHandler {

    private static final String SAVE_LOCATION = "playerSolution/Solution.java";
    private static final String CLASS_PATH = "playerSolution.Solution";

    private boolean canRun = false;

    private void compile() throws InlineCompilerException {

        File rawFile = new File(SAVE_LOCATION);
        JavaCompiler compile = ToolProvider.getSystemJavaCompiler();

        DiagnosticCollector<JavaFileObject> diagnostics = new DiagnosticCollector<>();
        StandardJavaFileManager jfm = compile.getStandardFileManager(diagnostics, Locale.ENGLISH, null);
        Iterable<? extends JavaFileObject> iterbl = jfm.getJavaFileObjectsFromFiles(Arrays.asList(rawFile));

        //Check what this does actually???
        List<String> classPath = new ArrayList<>();
        classPath.add("-classpath");
        classPath.add(System.getProperty("java.class.path") + ";dist/SolutionHandler.jar");

        JavaCompiler.CompilationTask task = compile.getTask(null, jfm, diagnostics, null, null, iterbl);

        if (!task.call()) {
            throw new InlineCompilerException(diagnostics.getDiagnostics());
        }

    }

    public synchronized void compileSolution(String SourceCode) throws IOException,
                                                                       InlineCompilerException,
                                                                       InterruptedException 
    {

        canRun = false;
        writeFile(SourceCode);
        compile();

        canRun = true;
        notifyAll();

    }

    private void writeFile(String SourceCode) throws IOException {

        File rawFileLocation = new File(SAVE_LOCATION);

        try (Writer rawWrite = new FileWriter(rawFileLocation)) {
            rawWrite.write(SourceCode);
            rawWrite.flush();
        }

    }

    public synchronized void startInstance() throws ClassNotFoundException,
                                                    NoSuchMethodException,
                                                    InstantiationException,
                                                    IllegalAccessException,
                                                    IllegalArgumentException,
                                                    InvocationTargetException,
                                                    InterruptedException,
                                                    MalformedURLException 
    {

        while (!canRun) {
            System.out.println("A runner thread is waiting");
            wait();

        }

        URL url = new File(".").toURI().toURL();
        Class<?> loadClass = Class.forName(CLASS_PATH, true, new URLClassLoader(new URL[]{url}));

        Method virtualMainMethod = loadClass.getMethod("virtualMain");

        Object o = loadClass.newInstance();

        virtualMainMethod.invoke(o, new Object[0]); //invoke method using no args

        canRun = false;

    }

    //  public static interface GameVirtualMain { public void virtualMain();}
    public Class getCompiledClass() throws ClassNotFoundException, MalformedURLException {
        URL url = new File(".").toURI().toURL();
        Class<?> loadClass = Class.forName(CLASS_PATH, true, new URLClassLoader(new URL[]{url}));
        return loadClass;

    }

}
