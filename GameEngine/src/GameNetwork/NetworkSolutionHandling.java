/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package GameNetwork;

import CustomExceptions.GameDrawException;
import CustomExceptions.AbandonedGameException;
import CustomExceptions.LobbyFullException;
import Engine.GameInstance;
import Engine.Player;
import java.rmi.NotBoundException;
import java.rmi.Remote;
import java.rmi.RemoteException;
import java.util.UUID;
import CustomExceptions.NoSuchGameInstanceException;
import java.util.LinkedList;

/**
 *
 * @author Pig3on
 */
public interface NetworkSolutionHandling extends Remote {
    
    public  GameInstance startGame(Player p,UUID gameInstanceID) throws RemoteException,LobbyFullException,NotBoundException;
    
    public boolean isOpponentReady(UUID gameInstanceID) throws RemoteException,NotBoundException,AbandonedGameException,NoSuchGameInstanceException;
    
    public void sendSolution(UUID gameInstanceID,Player p) throws RemoteException,AbandonedGameException;
    
    public GameInstance getWinner(UUID gameInstanceID)throws RemoteException,NoSuchGameInstanceException,AbandonedGameException,GameDrawException;
    
    public void declareWinner(Player p,GameInstance instance) throws RemoteException;
    
    public void requestLogout(Player p,UUID gameInstanceID)throws RemoteException;
    
    public LinkedList<GameInstance> getInstances()throws RemoteException;
    
    public void setGameDraw(UUID gameInstanceID)throws RemoteException;
}
