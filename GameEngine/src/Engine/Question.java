/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Engine;

import java.io.Serializable;
import java.util.UUID;

/**
 *
 * @author Pig3on
 */
public class Question implements Serializable {
    private UUID QuestionID;
    private String questionText;
    private String questionSolution;

    public Question() {
    }

    
    
    public Question(String questionText, String questionSolution) {
        this.QuestionID = UUID.randomUUID();
        this.questionText = questionText;
        this.questionSolution = questionSolution;
    }

    public UUID getQuestionID() {
        return QuestionID;
    }

    public void setQuestionID(UUID QuestionID) {
        this.QuestionID = QuestionID;
    }

    public String getQuestionText() {
        return questionText;
    }

    public void setQuestionText(String questionText) {
        this.questionText = questionText;
    }

    public String getQuestionSolution() {
        return questionSolution;
    }

    public void setQuestionSolution(String questionSolution) {
        this.questionSolution = questionSolution;
    }
    
    
    
    
    
}
