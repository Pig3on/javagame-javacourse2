/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Engine;

import java.io.Serializable;
import java.time.LocalTime;
import java.util.LinkedList;
import java.util.List;

/**
 *
 * @author Pig3on
 */
public class Solution implements Serializable {

    private final String sourceCode;

    private final LocalTime TimeOfCreation;

    private final LinkedList<Move> moves;

    private final Long elapsedTime;

    public Solution(String SourceCode, LocalTime TimeOfCreation, LinkedList<Move> moves, long elapsedTime) {
        this.moves = moves;

        this.sourceCode = SourceCode;
        this.TimeOfCreation = TimeOfCreation;
        this.elapsedTime = elapsedTime;
    }

    public String getSourceCode() {
        return sourceCode;
    }

    public LocalTime getTimeOfCreation() {
        return TimeOfCreation;
    }

    public long getElapsedTime() {
        return elapsedTime;
    }

}
