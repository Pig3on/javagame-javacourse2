/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Engine;

import CustomExceptions.AbandonedGameException;
import CustomExceptions.GameDrawException;
import CustomExceptions.LobbyFullException;
import CustomThreads.ClassRunnable;
import CustomThreads.CompileClassThread;
import CustomExceptions.InlineCompilerException;
import CustomExceptions.NoSuchGameInstanceException;
import DML.NetworkHandler;
import DML.SolutionHandler;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;

import java.net.MalformedURLException;
import java.rmi.NotBoundException;
import java.rmi.RemoteException;
import java.time.LocalTime;
import java.util.LinkedList;
import java.util.UUID;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;
import java.util.concurrent.ThreadFactory;

/**
 *
 * @author Pig3on
 */
public class GameManager {

    //TODO-- Implement a Logger Class for Errors Later!!!!
    SolutionHandler handler;
    NetworkHandler netHandler;
    Player currentPlayer;
    GameInstance currentGameInstance;

    public GameManager(Player currentPlayer) throws RemoteException, NotBoundException {
        this.handler = new SolutionHandler();
        this.netHandler = new NetworkHandler();
        this.currentPlayer = currentPlayer;
    }

    public GameManager() throws RemoteException, NotBoundException {
        handler = new SolutionHandler();
        netHandler = new NetworkHandler();
    }

    public GameInstance getCurrentGameInstance() {
        return currentGameInstance;
    }

    public void setCurrentGameInstance(GameInstance currentGameInstance) {
        this.currentGameInstance = currentGameInstance;
    }

    public void runSolution(String SourceCode) throws InterruptedException, ClassNotFoundException, InlineCompilerException {

        ExecutorService executor = Executors.newFixedThreadPool(4);
        Runnable runInstance = new ClassRunnable(handler);
        Future<?> future = executor.submit(new CompileClassThread(handler, SourceCode));
        executor.execute(runInstance);

        try {

            future.get();

        } catch (ExecutionException exe) {
            System.out.println("\u001B[31mCompiler error accured: runner threads shutting down");

            executor.shutdownNow();
            if (exe.getCause() instanceof InlineCompilerException) {
                InlineCompilerException exeption = (InlineCompilerException) exe.getCause();
                throw new InlineCompilerException(exeption.getDiagnosics());
            }
        }

        executor.shutdown();

    }

    public void submitCode(String sourceCode, LinkedList<Move> moves, File savePath, long elapsedTime) throws InterruptedException, ClassNotFoundException, MalformedURLException, InstantiationException, IllegalAccessException, InlineCompilerException, IOException, RemoteException, AbandonedGameException {

        Solution playerSolution = new Solution(sourceCode, LocalTime.now(), moves, elapsedTime);

        ExecutorService executor = Executors.newFixedThreadPool(4, new ThreadFactory() {
            @Override
            public Thread newThread(Runnable r) {
                Thread t = new Thread(r);
                t.setName("Compiler" + t.getId());
                return t;
            }
        });

        Future<?> future = executor.submit(new CompileClassThread(handler, sourceCode));
        try {
            future.get();
        } catch (ExecutionException exe) {
            System.out.println("\u001B[31mCompiler error accured: runner threads shutting down");

            executor.shutdownNow();
            if (exe.getCause() instanceof InlineCompilerException) {
                InlineCompilerException exeption = (InlineCompilerException) exe.getCause();
                throw new InlineCompilerException(exeption.getDiagnosics());
            }
        }

        executor.shutdown();

        //setup payload
        currentPlayer.setPlayerSolution(playerSolution);
        saveGame(playerSolution, savePath);
        netHandler.sendSolution(currentGameInstance.getGameInstanceID(), currentPlayer);
    }

    private void saveGame(Solution payload, File savePath) throws FileNotFoundException, IOException {

        FileOutputStream output = new FileOutputStream(savePath);
        ObjectOutputStream outputObject = new ObjectOutputStream(output);

        outputObject.writeObject(payload);

    }

    public Solution loadGame(File loadedFIle) throws FileNotFoundException, IOException, ClassNotFoundException {
        FileInputStream input = new FileInputStream(loadedFIle);
        ObjectInputStream inputObject = new ObjectInputStream(input);
        Object payLoadinstance = inputObject.readObject();
        if (payLoadinstance instanceof Solution) {
            return (Solution) payLoadinstance;
        } else {
            throw new ClassNotFoundException();
        }
    }

    public Class getCompiledClass() throws ClassNotFoundException, MalformedURLException {

        return handler.getCompiledClass();
    }

    public void startGame(UUID gameInstanceID) throws RemoteException, NotBoundException, LobbyFullException {

        currentGameInstance = netHandler.startGame(currentPlayer, gameInstanceID);

    }

    public boolean isOpponentReady() throws RemoteException, NotBoundException, AbandonedGameException, NoSuchGameInstanceException {
        return netHandler.isOpponentReady(currentGameInstance.getGameInstanceID());
    }

    public GameInstance getNextGameInstance() throws IOException, ClassNotFoundException {
        return netHandler.getNextGameInsance();
    }

    public GameInstance getWinner() throws RemoteException, NoSuchGameInstanceException, AbandonedGameException, GameDrawException {
        return netHandler.getWinner(currentGameInstance.getGameInstanceID());
    }

    public Player getCurrentPlayer() {
        return currentPlayer;
    }

    public void declareWinner(Player p, GameInstance instance) throws RemoteException {
        netHandler.declareWinner(p, instance);
    }

    public void requestLogout() throws RemoteException {
        netHandler.requestLogout(currentPlayer, currentGameInstance.getGameInstanceID());
    }

    public LinkedList<GameInstance> getInstances() throws RemoteException {
        return netHandler.getInstances();
    }

    public void setGameDraw(UUID gameInstanceID) throws RemoteException {

        netHandler.setGameDraw(gameInstanceID);

    }

}
