/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package CustomExceptions;

import java.util.List;
import javax.tools.Diagnostic;
import javax.tools.JavaFileObject;

/**
 *
 * @author Pig3on
 */
public class InlineCompilerException extends Exception{

    private final List<Diagnostic<? extends JavaFileObject>> diagnosics;

    public List<Diagnostic<? extends JavaFileObject>> getDiagnosics() {
        return diagnosics;
    }

    public InlineCompilerException(List<Diagnostic<? extends JavaFileObject>> diagnosics) {
        this.diagnosics = diagnosics;
    }

   
    
    
    
    
}
  
