<?xml version="1.0" encoding="utf-8" ?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
    <xsl:template match="/">
        <html>
        <body >
            <div style="margin:auto;background-position:center;width:600px;height:800px;background-image:url('./LogoSpeedOpacity.png');background-repeat:repeat-y;">


                <div style="margin:auto; width:600px;font-family:Calibri">

                    <table border="0">
                        <tr>
                            <td>
                                <h1 style="text-align:center">
                                    REPORT
                                </h1>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <h2 style="text-align:center">
                                    Player:
                                    <xsl:value-of select="Player/Username" />
                                </h2>
                            </td>
                        </tr>

                        <tr>
                            <td>
                                <div style="width:600px;text-align:center;font-family:Calibri">
                                    <xsl:value-of select="Player/@PlayerID" />


                                    has
                                    <xsl:if test="Player/Winner=1">
                                        <strong style="color:green">WON</strong>
                                    </xsl:if>
                                    <xsl:if test="Player/Winner=0">
                                        <strong style="color:red">LOST</strong>
                                    </xsl:if>

                                    in Game: <xsl:value-of select="Player/GameInstanceID" />
                                </div>
                            </td>
                        </tr>

                        <tr>


                            <td>
                                <p>
                                    Task Given:
                                    <div>
                                        <xsl:value-of select="Player/Question" />
                                    </div>
                                </p>
                            </td>

                        </tr>
                    </table>
                </div>
                <div style="margin:auto; border:1px solid black;width:600px;">
                    <p>Solution</p>

                    <table border="0" style="width:600px">

                        <tr>
                            <td colspan="2">

                                <div style="font-family:Consolas">

                                    <pre><xsl:value-of select="Player/Solution/Source" /></pre>

                                </div>

                            </td>
                        </tr>

                    </table>
                </div>

                <div style="margin:auto; width:600px;text-align:center;font-family:Calibri">
                    <table style="margin: auto; width: 600px;">
                        <tr>
                            <td colspan="2" style="color:red; font-weight:bolder">
                                Time to complete:
                                <xsl:value-of select="Player/Solution/ElapsedTime" />
                            </td>
                        </tr>
                        <tr>
                            <td colspan="2" style="padding-bottom:50px">
                                JudgeComment:
                                <xsl:value-of select="Player/JudgeComment" />
                            </td>
                        </tr>

                        <tr style="text-align:center">
                            <td style="border-top:1px solid black; margin-top:1px">
                                Player signature
                            </td>
                            <td style="border-top:1px solid black;margin-top:1px">
                                Judge signature
                            </td>
                        </tr>
                    </table>


                </div>
            </div>
        </body>
    </html>
</xsl:template>
</xsl:stylesheet>