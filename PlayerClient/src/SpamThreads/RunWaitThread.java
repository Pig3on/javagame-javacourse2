/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package SpamThreads;

import CustomExceptions.AbandonedGameException;
import CustomExceptions.NoSuchGameInstanceException;
import Engine.GameManager;
import com.google.common.base.Stopwatch;
import CustomExceptions.GameConnectionTimeoutException;
import java.rmi.NotBoundException;
import java.rmi.RemoteException;
import java.util.logging.Level;
import java.util.logging.Logger;
import javafx.application.Platform;
import javafx.scene.control.Button;
import javafx.scene.control.TextArea;
import org.fxmisc.richtext.CodeArea;

/**
 *
 * @author Pig3on
 */
public class RunWaitThread implements Runnable {

    GameManager mngr;
    boolean once = true;
    private final Button btnSubmit;
    private final Button btnLoadClass;
    private final CodeArea txtArea;
    private final CodeArea errorArea;
    private int timeout = 60;

    public RunWaitThread(GameManager mngr, Button btnSubmit, Button btnLoadClass, CodeArea txtArea, CodeArea errorArea) {
        this.mngr = mngr;
        this.btnSubmit = btnSubmit;
        this.btnLoadClass = btnLoadClass;
        this.txtArea = txtArea;
        this.errorArea = errorArea;
    }

    @Override
    public void run() {

        try {
            while (!mngr.isOpponentReady()) {
                timeout = timeout - 1;

                if (timeout < 0) {
                    throw new GameConnectionTimeoutException("Player 2 did not connect in time");
                }
                Thread.sleep(1000);

            }
            Platform.runLater(playerReady());
        } catch (RemoteException ex) {
            errorArea.appendText("\n Server appears to be offline \n");
            errorArea.appendText(ex.getMessage());
        } catch (NotBoundException | InterruptedException | AbandonedGameException | NoSuchGameInstanceException ex) {
            Logger.getLogger(RunWaitThread.class.getName()).log(Level.SEVERE, null, ex);
        } catch (GameConnectionTimeoutException ex) {
            Logger.getLogger(RunWaitThread.class.getName()).log(Level.SEVERE, null, ex);
            Platform.runLater(timeoutExeption());
        }

    }

    private Runnable playerReady() {
        return new Runnable() {
            @Override
            public void run() {
                btnSubmit.setDisable(false);
                btnLoadClass.setDisable(false);
                txtArea.setEditable(true);
                errorArea.replaceText("");
                txtArea.setFocusTraversable(true);
            }
        };
    }

    private Runnable timeoutExeption() { //TODO implement this
        return new Runnable() {
            @Override
            public void run() {
                btnSubmit.setDisable(true);
                btnLoadClass.setDisable(true);
                txtArea.setEditable(false);

                errorArea.replaceText("Player 2 did not connect in time");
            }
        };
    }

}
