/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package SpamThreads;

import CustomExceptions.AbandonedGameException;
import CustomExceptions.GameDrawException;
import CustomExceptions.NoSuchGameInstanceException;
import Engine.GameInstance;
import Engine.GameManager;
import Engine.Player;
import java.rmi.RemoteException;
import java.util.logging.Level;
import java.util.logging.Logger;
import javafx.application.Platform;
import javafx.scene.control.Alert;
import org.fxmisc.richtext.CodeArea;

/**
 *
 * @author Pig3on
 */
public class WinnerListener implements Runnable {

    GameManager mngr;
    CodeArea winner;
    Alert broadcast = new Alert(Alert.AlertType.INFORMATION);
    Player currentPlayer;
    Player winnerPlayer = null;
    GameInstance currentInstance = null;

    public WinnerListener(GameManager mngr, CodeArea winner, Player currentPlayer) {
        this.mngr = mngr;
        this.winner = winner;
        this.currentPlayer = currentPlayer;
    }

    @Override
    public void run() {

        try {
            while ((currentInstance = mngr.getWinner()) == null) {
                Thread.sleep(5000);
            }
            Platform.runLater(announceWinner());
        } catch (RemoteException ex) {
            Logger.getLogger(WinnerListener.class.getName()).log(Level.SEVERE, null, ex);
        } catch (NoSuchGameInstanceException ex) {
            Logger.getLogger(WinnerListener.class.getName()).log(Level.SEVERE, null, ex);
        } catch (InterruptedException ex) {
            Logger.getLogger(WinnerListener.class.getName()).log(Level.SEVERE, null, ex);
        } catch (AbandonedGameException ex) {
            Logger.getLogger(WinnerListener.class.getName()).log(Level.SEVERE, null, ex);
        } catch (GameDrawException ex) {
            Platform.runLater(gameDrawExceptionHandle());
        }

    }

    private Runnable announceWinner() {
        return new Runnable() {
            @Override
            public void run() {

                winnerPlayer = currentInstance.getWinnner();

                for (Player player : currentInstance.getPlayerList()) {

                    if (player.getPlayerID().equals(currentPlayer.getPlayerID())) {
                        currentPlayer.setDescription(player.getDescription());
                    }
                }

                if (!currentPlayer.getPlayerID().equals(winnerPlayer.getPlayerID())) { //LOST

                    broadcast.setTitle("You lost");
                    broadcast.setHeaderText("You lost to " + winnerPlayer.getUsername());
                    winner.replaceText("You lost to " + winnerPlayer.getUsername() + " " + winnerPlayer.getPlayerID().toString());

                    if (currentPlayer.getDescription() != null) {
                        broadcast.setContentText("Judge Comment: " + currentPlayer.getDescription());
                        winner.appendText("Judge Comment: " + currentPlayer.getDescription());
                    } else {
                        broadcast.setContentText("**No Comments**");
                        winner.appendText("**No Comments**");
                    }

                    broadcast.showAndWait();

                } else { //WON
                    winner.replaceText("You WON!!!! CONGRATULATIONS!!!!! \n");
                    winner.appendText("you won the game with the time of: " + winnerPlayer.getPlayerSolution().getElapsedTime() + "ms \n");
                    broadcast.setTitle("WINNER WINNER CHICKEN DINNER!!!!");
                    broadcast.setHeaderText("Congratulations!!! You WON!!!!!");
                    broadcast.setContentText("Judge Comment: " + winnerPlayer.getDescription());
                    broadcast.showAndWait();

                }

            }
        };
    }

    private Runnable gameDrawExceptionHandle() {

        return new Runnable() {
            @Override
            public void run() {
                winner.replaceText("Game is a draw \n");

                broadcast.setTitle("Game is a Draw");
                broadcast.setHeaderText("Its a Draw");

                broadcast.showAndWait();
            }
        };

    }

}
