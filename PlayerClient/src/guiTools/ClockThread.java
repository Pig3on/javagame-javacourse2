/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package guiTools;

import java.time.LocalTime;
import javafx.scene.control.Label;

/**
 *
 * @author Pig3on
 */
public class ClockThread implements Runnable {

    private final Label cloclLbl;

    public ClockThread(Label cloclLbl) {

        this.cloclLbl = cloclLbl;
    }

    @Override
    public void run() {

        LocalTime currentTime = LocalTime.now();
        cloclLbl.setText(String.format("%d:%d:%d", currentTime.getHour(), currentTime.getMinute(), currentTime.getSecond()));

    }

}
