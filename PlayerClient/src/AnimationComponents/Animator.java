/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package AnimationComponents;

import javafx.animation.AnimationTimer;
import javafx.scene.control.Button;

/**
 *
 * @author Pig3on
 */
public abstract class Animator extends AnimationTimer {

    protected AnimatedImage image = null;
    private final long startNanoTime;
    
    

    public void setImage(AnimatedImage image) {
        this.image = image;
    }

    
    
    public Animator() {
        this.startNanoTime = System.nanoTime(); 
    }
    
    public abstract void handleAnimation(double t);

    @Override
    public void handle(long now) {
        double t = (now - startNanoTime)/1000000000.0;
        handleAnimation(t);
    }
    
    
    
   
    
}
