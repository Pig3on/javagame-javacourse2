/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package AnimationComponents;

import java.util.LinkedList;
import javafx.scene.canvas.Canvas;
import javafx.scene.canvas.GraphicsContext;
import javafx.scene.paint.Color;

/**
 *
 * @author Pig3on
 */
public class CanvasAnimator extends Animator {

    private CanvasPane canvasPane;
    private GraphicsContext gc;
    LinkedList<AnimatedImage> animatedImages;

    public CanvasAnimator(CanvasPane canvasPane) {
        this.canvasPane = canvasPane;
        gc = canvasPane.getCanvas().getGraphicsContext2D();
        animatedImages = new LinkedList<>();

    }

    public void setAnimatedImages(LinkedList<AnimatedImage> animatedImages) {
        this.animatedImages = animatedImages;
    }

    @Override
    public void handleAnimation(double t) {
        if (animatedImages != null) {
            double widthImage = canvasPane.getCanvas().getWidth();
            double heightImage = canvasPane.getCanvas().getHeight();

            for (AnimatedImage animatedImage : animatedImages) {
                image = animatedImage;
                
                if (image.getFillRect() != null) {
                    gc.setFill(animatedImage.getFillRect());
                    gc.fillRect(0, 0, (int) widthImage, (int) heightImage);
                } else if (image.getFrames().length == 1) {
                    gc.drawImage(animatedImage.getFrames()[0], 0, 0, (int) widthImage, (int) heightImage);
                } else {
                    gc.drawImage(image.getFrame(t), 0, 0, (int) widthImage, (int) heightImage);
                }
                

            }
        }

    }

}
